# Upgrade ECK & Elastic, Kibana, Fleet, APM

Note check version compat first to match ECK operator with your kube version

https://www.elastic.co/guide/en/cloud-on-k8s/2.9/k8s_supported_versions.html

Then check your kube version and select ECK operator to match your kube verion

This case kube version is 1.23.6, maximum version ECK is 2.7

### 1. Check current ECK operator verion

        kubectl get sts -n elastic-system elastic-operator -oyaml

Scroll up to see version

![Alt text](image-3.png)

### 2. Upgrade ECK operator from 2.6.1 to 2.7

    kubectl apply -f https://download.elastic.co/downloads/eck/2.7.0/crds.yaml

    kubectl apply -f https://download.elastic.co/downloads/eck/2.7.0/operator.yaml

Check po and sts whether update to new version from no. 1

![Alt text](image.png)

### 3. Edit elastic, kibana, fleet, apm in yaml from version 8.5.0 to 8.11.0

    kubectl apply -f elastic-kibana-fleet.yaml

    kubectl apply -f apm-server.yaml

Wait until all elastic, kibana, fleet, apm rolling update to 8.11 and check version each of them

kubectl get sts elasticsearch-es-default -oyaml

![Alt text](image-1.png)

### 4. Try login to kibana UI and go to fleet to see agent version change from 8.5 to 8.11

![Alt text](image-2.png)